import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { map } from 'rxjs/Operators';
import { People } from '../models/people';
import { of } from 'rxjs';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  people;

  constructor(private data: DataService) {
    this.data.getJSON().subscribe(x => {
      console.log(x);
    });
  }

  ngOnInit() {
  }

}
