import { Component, OnInit, Input, Output } from '@angular/core';
import { DataService } from '../services/data.service';
import { map } from 'rxjs/Operators';
import { People } from '../models/people';
import { of, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { EventEmitter } from 'events';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent {

  @Input('people') people;
  @Output() change = new EventEmitter();

  constructor() {

  }

  onClick() {
    console.log('changed');
    this.change.emit(this.people);
  }



}
