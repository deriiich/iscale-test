import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { map } from 'rxjs/Operators';
import { People } from '../models/people';
import { of } from 'rxjs';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  people: any[] = [];

  constructor(private data: DataService) {
    this.data.getPeople().subscribe(objects => {
      this.people = objects;
    });
  }

  ngOnInit() {
  }

}
