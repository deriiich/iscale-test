import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent {

  @Input('people') people;
  @Output() change = new EventEmitter();


  constructor() {

  }

  onClick(value) {
    this.change.emit(value);
  }



}
