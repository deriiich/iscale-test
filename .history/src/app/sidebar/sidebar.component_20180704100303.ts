import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  people$ = [];

  constructor(private data: DataService) {
    this.data.getPeople().subscribe(people => {
      this.people$ = people;
    });
  }

  ngOnInit() {
  }

}
