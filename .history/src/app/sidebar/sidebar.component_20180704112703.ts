import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { map } from 'rxjs/Operators';
import { People } from '../models/people';
import { of, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent {

  people: People[];

  constructor(private http: HttpClient) {
    this.getJSON().subscribe(data => {
      console.log(data);
      this.people = data;

    });
  }

  public getJSON(): Observable<any> {
    return this.http.get('./assets/people.json');
    // return this.http.get("https://jsonplaceholder.typicode.com/posts");
  }

}
