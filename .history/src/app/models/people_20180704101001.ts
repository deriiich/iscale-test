export interface People {
    name: string;
    email: string;
    isAdmin: boolean;
}
