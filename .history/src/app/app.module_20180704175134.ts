import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { DataService } from './services/data.service';
import { ProfileComponent } from './profile/profile.component';
import { MainComponent } from './main/main.component';
import { RouterModule } from '@angular/router';

import { Angular2FontawesomeModule } from 'angular2-fontawesome/angular2-fontawesome';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SidebarComponent,
    ProfileComponent,
    MainComponent,

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    Angular2FontawesomeModule,
    NgbModule.forRoot(),
    RouterModule.forRoot([
      { path: '', component: ProfileComponent },
    ])
  ],
  providers: [
    DataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
