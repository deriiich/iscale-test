import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/Operators';
import { Observable, of } from 'rxjs';
import { People } from '../models/people';


@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) {

  }

  public getPeople(): Observable<People> {
    return this.http.get('./assets/people.json');
  }
}
