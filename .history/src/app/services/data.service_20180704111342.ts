import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/Operators';
import { Observable, of } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) {
    this.getPeople().pipe(map(data => {
      return data;
    }));
  }

  public getPeople(): Observable<any> {
    return this.http.get('./assets/people.json');
  }
}
