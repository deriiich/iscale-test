import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { People } from '../models/people';


@Injectable({
  providedIn: 'root'
})
export class DataService {
  people;

  constructor(private http: HttpClient) {
  }


  getPeople(): Observable<People[]> {
    this.http.get('./assets/people.json').subscribe(x => {
      this.people = x;
    });
    return this.people;

  }
}
