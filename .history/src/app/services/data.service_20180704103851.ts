import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { People } from '../models/people';


@Injectable({
  providedIn: 'root'
})
export class DataService {
  people;

  constructor(private url: string, private http: HttpClient) { }


  getAll() {
    return this.http.get(this.url)
      .map(response => response.json())
      .catch(this.handleError);
  }
}
