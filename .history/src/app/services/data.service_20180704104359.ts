import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/Operators';


@Injectable({
  providedIn: 'root'
})
export class DataService {
  people;

  constructor(private url: string, private http: HttpClient) {

  }


  getAll() {
    return this.http.get('./assets/people.json').pipe(map(response => response));
  }
}
