import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private httpClient: HttpClient) {
    this.getPeople().subscribe(data => {
      console.log(data);
    });
  }

  getPeople() {
    return this.httpClient.get('./people.json');
  }
}
