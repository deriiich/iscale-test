import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private httpClient: HttpClient) {

  }
  ./data.service
  getPeople() {
    return this.httpClient.get('./people.json');
  }
}
