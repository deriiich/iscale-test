import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { People } from '../models/people';


@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) {
    this.getPeople().subscribe(data => {
      console.log(data);
    });
  }


  getPeople(): Observable<People[]> {
    return this.http.get('./assets/people.json');

  }
}
