import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { People } from '../models/people';


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  @Input('person') person: People;
  @Output() filter = new EventEmitter();


  constructor() {
  }

  ngOnInit() {
  }
  onEnter(value) {
    console.log(value);
  }
  filtering(query) {
    console.log(query.value);
    this.filter.emit(query.value);
  }

  removeSearch(search: HTMLInputElement) {
    search.value = '';
    this.filter.emit(search);

  }
}
