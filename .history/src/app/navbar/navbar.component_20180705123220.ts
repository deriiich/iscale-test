import { Component, OnInit, Input } from '@angular/core';
import { People } from '../models/people';


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  @Input('person') person: People;
  constructor() {
  }

  ngOnInit() {
  }

}
