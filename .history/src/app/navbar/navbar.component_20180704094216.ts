import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private data: DataService) {
    this.data.getPeople().subscribe(x => console.log(x));
  }

  ngOnInit() {
  }


}
