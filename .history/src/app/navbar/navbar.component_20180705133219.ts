import { Component, OnInit, Input } from '@angular/core';
import { People } from '../models/people';


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  @Input('person') person: People;

  constructor() {
  }

  ngOnInit() {
  }

  filter(query: string) {
    console.log(query);
    // const filteredProducts = (query) ?
    //   this.products.filter(p => p.price.toString().toLowerCase().includes(query.toLowerCase()) ||
    //     p.title.toLowerCase().includes(query.toLowerCase()) ||
    //     p.category.toLowerCase().includes(query.toLowerCase())) :
    //   this.products;
    // this.initializeTable(filteredProducts);
  }

}
