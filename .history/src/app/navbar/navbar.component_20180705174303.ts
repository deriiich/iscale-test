import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { People } from '../models/people';


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  @Input('person') person: People;
  @Output() filtering = new EventEmitter();
  @Output() choosePerson = new EventEmitter();

  isCollapse;

  constructor() {
  }

  ngOnInit() {
  }


  filter(query) {
    this.filtering.emit(query.value);
  }

  removeSearch(search) {
    search.value = '';
    this.filtering.emit(search.value);

  }


}
