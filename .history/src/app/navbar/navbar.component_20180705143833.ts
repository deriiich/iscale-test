import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { People } from '../models/people';


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  @Input('person') person: People;
  @Output() filtered = new EventEmitter();


  constructor() {
  }

  ngOnInit() {
  }

  filter(query: string) {
   
    this.filtered.emit(query);
  }

  removeSearch(search: HTMLInputElement) {
    console.log(search.value);
  }
}
