import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { People } from '../models/people';


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  @Input('person') person: People;
  @Output() filter = new EventEmitter();
  @Output() enterPerson = new EventEmitter();

  isCollapse;

  constructor() {
  }

  ngOnInit() {
  }
  choose(value) {
    console.log(value.value);
  }
  filtering(query) {
    this.filter.emit(query.value);
  }

  removeSearch(search) {
    search.value = '';
    this.filter.emit(search.value);

  }


}
