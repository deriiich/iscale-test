import { Component, OnInit, Input, Output } from '@angular/core';
import { People } from '../models/people';
import { DataService } from '../services/data.service';
import { createHostListener } from '@angular/compiler/src/core';
import { EventEmitter } from 'events';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {


  people$: People[];
  person;
  // filteredPerson: any[] = [];

  constructor(private dataService: DataService) {

  }

  ngOnInit() {
    this.dataService.getPeople().subscribe(data => {
      this.filteredPerson = data.People;

    });

  }

  onChosenUserFromSidebar(value) {
    this.person = value;
  }


  onFiltering(query) {
    this.dataService.getPeople().subscribe(data => {

      const filterPerson = (query) ?
        this.people$.filter(p => p.name.toString().toLowerCase().includes(query.toLowerCase())) :
        this.people$ = data.People;
    });

    // this.person = { name: 'Derich', Description: 'HAHAHAHHA' };



    // console.log(this.person);
    if (this.people$.filter(p => p.name.toString().toLowerCase().includes(query.toLowerCase()))) {
      console.log('yes');
    }

  }

  onChoosePerson(query) {
  }
}
