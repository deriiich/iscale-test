import { Component, OnInit, Input, Output } from '@angular/core';
import { People } from '../models/people';
import { DataService } from '../services/data.service';
import { createHostListener } from '@angular/compiler/src/core';
import { EventEmitter } from 'events';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {


  people$: People[];
  persons: People[];
  filteredPersons: any[];


  constructor(private dataService: DataService) {
    this.dataService.getPeople().subscribe(data => {
      this.persons = this.filteredPersons = data.People;
    });


  }

  ngOnInit() {
    this.dataService.getPeople().subscribe(data => {
      this.filteredPersons = data.People;

    });

  }

  onChosenUserFromSidebar(value) {
    this.persons = value;
  }


  onFiltering(query) {

    // this.person = { name: 'Derich', Description: 'HAHAHAHHA' };
    this.filteredPersons = (query) ?
      this.persons.filter(p => p.name.toString().toLowerCase().includes(query.toLowerCase())) :
      this.persons;

    console.log(this.persons);


  }

  onChoosePerson(query) {
  }
}
