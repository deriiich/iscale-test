import { Component, OnInit, Input, Output } from '@angular/core';
import { People } from '../models/people';
import { DataService } from '../services/data.service';
import { createHostListener } from '@angular/compiler/src/core';
import { EventEmitter } from 'events';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  
  people$: People[];
  person;
  filteredPerson: any[] = [];

  constructor(private dataService: DataService) {
    this.dataService.getPeople().subscribe(data => {
      this.people$ = data.People;
    });


  }

  ngOnInit() {
    this.dataService.getPeople().subscribe(data => {
      this.filteredPerson = data.People;

    });

  }

  onChosenUserFromSidebar(value) {
    this.person = value;
  }


  onFiltering(query) {

    // this.person = { name: 'Derich', Description: 'HAHAHAHHA' };

    this.filteredPerson = (query) ?
      this.people$.filter(p => p.name.toString().toLowerCase().includes(query.toLowerCase())) :
      this.people$;

    console.log(this.filteredPerson);
    

  }

  onChoosePerson(query) {
  }
}
