import { Component, OnInit, Input, Output } from '@angular/core';
import { People } from '../models/people';
import { DataService } from '../services/data.service';
import { createHostListener } from '@angular/compiler/src/core';
import { EventEmitter } from 'events';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {


  people$: People[];
  persons: any[];
  filteredPersons: any[];
  filteredPerson: any[] = [];

  constructor(private dataService: DataService) {
    this.dataService.getPeople().subscribe(data => {
      this.persons = this.filteredPersons = data.People;
    });


  }

  ngOnInit() {
    this.dataService.getPeople().subscribe(data => {
      this.filteredPerson = data.People;

    });

  }

  onChosenUserFromSidebar(value) {
    this.person = value;
  }


  onFiltering(query) {

    // this.person = { name: 'Derich', Description: 'HAHAHAHHA' };
    this.filteredPersons
    this.person = (query) ?
      this.people$.filter(p => p.name.toString().toLowerCase().includes(query.toLowerCase())) :
      this.people$;

    console.log(this.person);


  }

  onChoosePerson(query) {
  }
}
