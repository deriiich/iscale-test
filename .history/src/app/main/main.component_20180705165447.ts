import { Component, OnInit, Input } from '@angular/core';
import { People } from '../models/people';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  people$: People[];
  person = {};
  filteredPerson: any[] = [];

  constructor(private dataService: DataService) {
    this.dataService.getPeople().subscribe(data => {
      this.people$ = data.People;
    });


  }

  ngOnInit() {
    this.dataService.getPeople().subscribe(data => {
      this.filteredPerson = data.People;

    });

  }

  onChanged(value) {
    this.person = value;
  }


  onFilter(query) {
    this.person = this.onChanged;
    // this.filteredPerson = (query) ?
    //   this.people$.filter(p => p.name.toString().toLowerCase().includes(query.toLowerCase())) :
    //   this.people$;

    // if (this.filteredPerson.length === 1) {
    //   this.person = this.filteredPerson;
    //   console.log(this.person);

    // }
  }

  onChoosePerson(person) {
    console.log('User has chose', person);
  }
}
