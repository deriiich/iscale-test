import { Component, OnInit, Input, Output } from '@angular/core';
import { People } from '../models/people';
import { DataService } from '../services/data.service';
import { createHostListener } from '@angular/compiler/src/core';
import { EventEmitter } from 'events';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {


  people: People[];
  persons: People[];
  filteredPerson: any[];
  person;

  constructor(private dataService: DataService) {
    this.dataService.getPeople().subscribe(data => {
      this.people = this.filteredPerson = data.People;
    });


  }

  ngOnInit() {
  }

  onChosenUserFromSidebar(value) {
    this.person = value;
  }


  onFiltering(query) {

    // this.person = { name: 'Derich', Description: 'HAHAHAHHA' };


    this.filteredPerson = (query) ?
      this.people.filter(p => p.name.toLowerCase().includes(query.toLowerCase())) :
      this.people;


  }

}
