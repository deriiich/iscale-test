import { Component, OnInit, Input } from '@angular/core';
import { People } from '../models/people';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  person;

  constructor() {

  }

  ngOnInit() {
  }

  onChanged(value) {
    this.person = value;
  }

}
